
class Cat:

	def __init__(self, name, meow):
		print("Создание объекта Cat")
		self.name = name
		self.meow = meow
		print("Объект создан со следующими атрибутами:")
		print("Meow = " + self.meow)
		print("Name = " + self.name)

	def make_sound(self):
		print(self.meow)


	def mrrr_sound(self):
		print("mrrrr")
		

cat = Cat("Naf", "Meoooow")
cat.make_sound()
cat.meow = "MeeeeOOOOOwwwwwwWW"
cat.name = "Marfa"
cat.make_sound()

class MetaClass(type):
    # выделение памяти для класса
    def __new__(cls, name, bases, dict):
        print("Создание нового класса {}".format(name))
        return type.__new__(cls, name, bases, dict)

    # инициализация класса
    def __init__(cls, name, bases, dict):
        print("Инициализация нового класса {}".format(name))
        return super(MetaClass, cls).__init__(name, bases, dict)

# порождение класса на основе метакласса
SomeClass = MetaClass("SomeClass", (), {})

# обычное наследование
class Child(SomeClass):
    def __init__(self, param):
        print(param)

# получение экземпляра класса
obj = Child("Hello")