from flask_admin.contrib.sqla import ModelView

class MyModelAdmin(ModelView):

    def on_model_change(self, form, model, is_created=False):
        model.picture = bytes(model.picture, 'utf-8')
        pass