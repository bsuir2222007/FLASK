from app import db
from flask_login import UserMixin




class UserCredentials(db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	login = db.Column(db.String(64), unique=True)
	password = db.Column(db.String(64))
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

	def __init__(self, login, password, user_id):
		self.login = login
		self.password = password
		self.user_id = user_id
	
class UserType(db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	type_name = db.Column(db.String(64))

class User(UserMixin, db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	name = db.Column(db.String(64), nullable=False)
	surname = db.Column(db.String(64), nullable=False)
	lastname = db.Column(db.String(64), nullable=False)
	type_id = db.Column('type_id', db.Integer, db.ForeignKey(UserType.id))

	def __repr__(self):
		return '<User {}>'.format(self.name)

	def __init__(self):
		self.type_id = 2

class Picture(db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	picture_url = db.Column(db.String, nullable = False)
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

	


	def __init__(self, pic, user_id):
		self.picture_url = pic
		self.user_id = user_id
		
		

class Subject(db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	name = db.Column(db.String(64))
	description = db.Column(db.String(64))

	def __init__(self, name, description):
		self.name = name
		self.description = description


class Group(db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	number = db.Column(db.Integer, unique=True)

class m2m_user_group(db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
	group_id = db.Column(db.Integer, db.ForeignKey('group.id'))


class m2m_subject_group(db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)		
	subject_id = db.Column(db.Integer, db.ForeignKey('subject.id'))
	professor_id = db.Column(db.Integer, db.ForeignKey('user.id'))
	group_id = db.Column(db.Integer, db.ForeignKey('group.id'))

	def __init__(self, subject_id, professor_id):
		self.subject_id = subject_id
		self.professor_id = professor_id

class UserSubject(db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	user_id = db.Column(db.Integer, db.ForeignKey('User.id'))
	subject_id = db.Column(db.Integer, db.ForeignKey('Subject.id'))
		

	def __repr__(self):
		return '<Type {}>'.format(self.type_name)

	
		