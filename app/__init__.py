from flask import Flask, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, current_user
from flask_admin import Admin, BaseView, AdminIndexView, expose
from flask_admin.contrib.sqla import ModelView



app = Flask(__name__)


UPLOAD_FOLDER = './app/static/uploads'

app.config.from_object('config')

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16*1024*1024

db = SQLAlchemy(app, session_options={"autoflush": False})

with app.app_context():
        db.create_all()

from app import views, model


class AdminView(ModelView):
	def is_accessible(self):
		if current_user.is_authenticated:
			if current_user.type_id == 3:
				return True
			else:
				return False
		return False

	def inaccessible_callback(self, name, **kwargs):
		return redirect(url_for('login'))




	
		

admin = Admin(app, template_mode = 'bootstrap4')
admin.add_view(AdminView(model.User, db.session, name = 'Users'))
admin.add_view(AdminView(model.Group, db.session, name = 'Group'))
admin.add_view(AdminView(model.Subject, db.session, name = 'Subject'))


login_manager = LoginManager()
login_manager.login_view = 'login'
login_manager.init_app(app)

@login_manager.user_loader
def load_user(user_id):
	return model.User.query.get(int(user_id))