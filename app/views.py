from app import app, model
from app import db
from flask import render_template, request, flash, redirect, url_for
import base64
from flask_login import login_user, current_user, login_required, logout_user
from .forms import LoginForm
from .forms import RegisterForm
from .forms import UserDataForm
from .forms import AddSubjectForm , AddGroupSelectorForm
import os
from werkzeug.utils import secure_filename
import uuid
import codecs

# basedir = os.path.abspath(os.path.dirname(__file__))

@app.route('/')
@app.route('/index')
def index():
	text = {'asdf': 'asdgf'}
	numbers = ["asds","afsd","asdsa","asdfasd", "asfasd", "agasdasd", "gasdasdf"]
	numbers_next = [numbers[3], numbers[5]]
	return render_template("index.html", out_text=text['asdf'], title="asdasdas", numbers=numbers_next)

@app.route('/base')
def person():
	return render_template("base.html")


@app.route('/register', methods=['GET','POST'])
def register():
	form = RegisterForm()
	if form.validate_on_submit():
		login = form.loginField.data
		userCred = model.UserCredentials.query.filter_by(login = login).first()
		if userCred != None:
			flash("Email exists")
			return render_template("register.html", form = form)
		else:
			password = form.passwordField.data
			user = model.User()
			db.session.add(user)
			db.session.commit()
			db.session.refresh(user)
			userCred = model.UserCredentials(login, password, user.id)
			db.session.add(userCred)
			db.session.commit()
			db.session.refresh(userCred)
			flash(userCred.id)
			login_user(user)
			return redirect(url_for('user_data'))
	return render_template("register.html", form = form)

@app.route('/group_info')
def group_info():
	group_id = request.args.get('group_id')
	group = model.Group.query.filter_by(id = group_id).first()
	m2m_u_g = model.m2m_user_group.query.filter_by(group_id = group.id).all()
	students = []
	for i in m2m_u_g:
		students.append(model.User.query.filter_by(id=i.user_id).first())	
	for i in students:
		if i == None:
			continue
		else:
			picture = model.Picture.query.filter_by(user_id = i.id).first()
			if picture:
				with open(os.path.join(app.config['UPLOAD_FOLDER'], picture.picture_url), 'rb') as file:
					picture = file.read()
				
				i.picture = base64.b64encode(picture).decode("utf-8")
	return render_template("group_info.html", group = group, students = students, user_type = current_user.type_id)

@app.route('/group')
def group():
	if current_user.is_authenticated:
		if current_user.type_id == 1:
			users = model.User.query.all()
			for us in users:
				

				picture = model.Picture.query.filter_by(user_id = us.id).first()
				if picture:
					
					with open(os.path.join(app.config['UPLOAD_FOLDER'], picture.picture_url), 'rb') as file:
						picture = file.read()

					us.picture = base64.b64encode(picture).decode("utf-8")
			return render_template("group.html", users = users)
		else:
			if model.m2m_user_group.query.filter_by(user_id = current_user.id).first() == None:
				return render_template("group.html", group = None, users = None)
			else:
				group_id = model.m2m_user_group.query.filter_by(user_id = current_user.id).first().group_id
				group = model.Group.query.filter_by(id = group_id).first()
				m2m_users = model.m2m_user_group.query.filter_by(group_id = group.id).all()
				users = []
				for user in m2m_users:
					users.append(model.User.query.filter_by(id = user.user_id).first())
				return render_template("group.html", group=group, users = users)
	else:
		return redirect(url_for('login'))

@app.route('/user_data', methods = ['GET', 'POST'])
def user_data():
	if current_user.is_authenticated:
		form = UserDataForm()
		pictures = model.Picture.query.filter_by(user_id = current_user.id).all()
		
		for pic in pictures:
			picture = ''
			flash(os.path.join(app.config['UPLOAD_FOLDER'], pic.picture_url))
			with open(os.path.join(app.config['UPLOAD_FOLDER'], pic.picture_url), 'rb') as file:
				picture = file.read()
			
			pic.picture = base64.b64encode(picture).decode("utf-8")
			
		if form.validate_on_submit():
			current_user.name = form.nameField.data
			current_user.surname = form.surnameField.data
			current_user.lastname = form.lastnameField.data
			db.session.add(current_user)
			db.session.commit()
			return redirect(url_for('user_data'))

		formid = request.args.get('form_type', 1, type=int)	
		if formid == 2:
			return render_template("user_data.html", form = form, flag = 2, pictures = pictures)

		return render_template("user_data.html", form = form, pictures = pictures)

# @app.route('/change_user_data', methods=['POST'])
# def change_user_data():
# 	form = UserDataForm()
# 	return render_template("user_data.html", form = form, flag = 3)

@app.route('/user_pic_upload', methods=['POST'])
def user_pic_upload():
	if 'pic' in request.files:
					file = request.files['pic']
					if file:
						secured_fn = secure_filename(file.filename)
						unique_fn = str(uuid.uuid4())
						file.save(os.path.join(app.config['UPLOAD_FOLDER'], unique_fn))
						picture = model.Picture(unique_fn, current_user.id)
						db.session.add(picture)
						db.session.commit()
	return redirect(url_for('user_data'))


@app.route('/user_pic_del', methods=['POST'])
def user_pic_del():
	pic_id = request.args.get('picture_to_del', 1, type=int)
	pic = model.Picture.query.filter_by(id = pic_id).first()
			

	db.session.query(model.Picture).where(model.Picture.id == pic.id).delete()
	db.session.commit()

	os.remove(os.path.join(app.config['UPLOAD_FOLDER'], pic.picture_url))

	return redirect("user_data")


@app.route('/user_info')
def user_info():
	if current_user.is_authenticated:
		m2m_u_s = []
		if current_user.type_id == 1:
			m2m_u_s = model.m2m_subject_group.query.filter_by(professor_id = current_user.id).all()
		else:
			group = model.m2m_user_group.query.filter_by(user_id = current_user.id).first()
			if group:
				m2m_u_s = model.m2m_subject_group.query.filter_by(group_id = group.group_id).all()
		subjects = []
		pictures = model.Picture.query.filter_by(user_id = current_user.id).all()
		
		for pic in pictures:
			picture = ''
			with open(os.path.join(app.config['UPLOAD_FOLDER'], pic.picture_url), 'rb') as file:
				picture = file.read()
			
			pic.picture = base64.b64encode(picture).decode("utf-8")
		for i in m2m_u_s:
			subjects.append(model.Subject.query.filter_by(id = i.subject_id).first())
		user_type = model.UserType.query.filter_by(id = current_user.type_id).first()
		return render_template('user_info.html', user=current_user , user_type=user_type , subjects=subjects, pictures = pictures)
	else:
		return redirect(url_for('login'))

@app.route('/subject_info')
def subject_info():
	subject_id = request.args.get('subject_id')
	subject = model.Subject.query.filter_by(id = subject_id).first()
	m2m_u_s = []
	if subject:
		m2m_u_s = model.m2m_subject_group.query.filter_by(subject_id = subject.id).all()
	groups = []

	for i in m2m_u_s:
		groups.append(model.Group.query.filter_by(id=i.group_id).first())	
	# for i in students:
	# 	if i == None:
	# 		continue
	# 	else:
	# 		picture = model.Picture.query.filter_by(user_id = i.id).first()
	# 		if picture:
	# 			with open(os.path.join(app.config['UPLOAD_FOLDER'], picture.picture_url), 'rb') as file:
	# 				picture = file.read()
				
	# 			i.picture = base64.b64encode(picture).decode("utf-8")

	return render_template('subject_info.html', subject=subject, user_type = current_user.type_id , groups = groups)

@app.route('/add_group', methods = ['GET', 'POST'])
def add_group():
	form = AddGroupSelectorForm()
	subject_id = request.args.get('subject_id')

	groups = model.Group.query.filter_by().all()


	form.groupIdField.choices = [(g.id, g.number) for g in groups]
	# if form.validate_on_submit():
	# 	g_id = form.groupIdField.data
	# 	flash("HELLO")
	g_id = form.groupIdField.data
	flash(g_id)
	if g_id:
		m2m_s_g = model.m2m_subject_group.query.filter_by(group_id = None, subject_id = subject_id, professor_id = current_user.id ).first()
		if m2m_s_g:
			m2m_s_g.group_id = g_id;
			db.session.add(m2m_s_g)
			db.session.commit()
		else:
			m2m_s_g = model.m2m_subject_group(subject_id, current_user.id)
			m2m_s_g.group_id = g_id;
			db.session.add(m2m_s_g)
			db.session.commit()
		return redirect(url_for('subject_info', subject_id = subject_id))

	return render_template('add_group.html', groups = groups, form = form)

@app.route('/login', methods = ['GET', 'POST'])
def login():
	if not current_user.is_authenticated : 
		form = LoginForm()
		if form.validate_on_submit():
				userCredentials = model.UserCredentials.query.filter_by(login = form.loginField.data).first()
				if userCredentials is None:
					flash('no such user')
				else:
					if userCredentials.password == form.passwordField.data:
						user = model.User.query.filter_by(id = userCredentials.user_id).first()
						if user:
							flash('Login good')
							user_type = model.UserType.query.filter_by(id = user.type_id).first()
							login_user(user)
							return redirect(url_for('user_info'))
						else:
							flash("bad user")
					else:
						flash('incorrect password')
				return render_template('login.html', title='login', form=form)
	else:
		return redirect(url_for('user_info'))
	return render_template('login.html', title='login', form=form)


@app.route('/add_subject', methods = ['GET', 'POST'])
def add_subject():
	form = AddSubjectForm()
	if form.validate_on_submit():
		subject = model.Subject(form.nameField.data, form.descriptionField.data)
		if model.Subject.query.filter_by(name = subject.name, description = subject.description).first():
			flash("Subject exists")
		else:
			db.session.add(subject)
			db.session.commit()
			db.session.refresh(subject)
			m2m_s_g = model.m2m_subject_group(subject.id, current_user.id)
			db.session.add(m2m_s_g)
			db.session.commit()
			return redirect(url_for('user_info'))
		flash(subject.name)
		
	return render_template('add_subject.html', form = form)

@app.route('/logout')
@login_required
def logout():
	logout_user()
	return redirect(url_for('login'))

	