from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SelectField
from wtforms.validators import DataRequired

class LoginForm(FlaskForm):
	loginField = StringField('loginField', validators=[DataRequired()])
	passwordField = StringField('passwordField', validators = [DataRequired()])

class RegisterForm(FlaskForm):
	loginField = StringField('loginField', validators=[DataRequired()])
	passwordField = StringField('passwordField', validators = [DataRequired()])

class UserDataForm(FlaskForm):
	nameField = StringField('nameField', validators=[DataRequired()])
	surnameField = StringField('surnameField', validators=[DataRequired()])
	lastnameField = StringField('lastnameField', validators=[DataRequired()])

class AddSubjectForm(FlaskForm):
	nameField = StringField('nameField', validators = [DataRequired()])
	descriptionField = StringField('descriptionField',validators = [DataRequired()])

class AddGroupSelectorForm(FlaskForm):
	groupIdField = SelectField('groupIdField', coerce = int, validators = [DataRequired()])
		